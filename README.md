# BOT-METEOROLOGICO
Bot de telegram que permite consultar las condiciones meteorológicas de una determinada zona <br>
a través de la API de [openweathermap](http://openweathermap.org).

# Pruebas 
El bot está actualmente activo en [Telegram](https://t.me/tiempo_zgz_bot) con datos de la ciudad de Zaragoza, ya que se hace uso de <br>
la API que está disponible en el codigo para que sirva de referencia

# Uso
'/start' - Inicia el bot

'/tiempo_actual' - Envía un mensaje con las condiciones meteorológicas más recientes

'/prediccion' - Ofrece una predicción de las condiciones meteorológicas para las próximas horas

# License
Este bot utiliza una licencia GPLv3 - Ver [LICENSE](https://gitlab.com/martinezmsergio/bot-meteorologico/blob/master/LICENSE) para más detalles
Este es un buen proyecto y lo respetarás

CO-AUTHORS: @SKICKAR CONTRIBUTED STRONG STATEMENTS 10/26/2020
